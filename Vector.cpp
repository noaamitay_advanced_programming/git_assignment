#include "Vector.h"

/*
	Input: an integer
	The function is a c'tor that initiate the fields with the integer
*/
Vector::Vector(int n)
{
	if (n < 2) //if the paramter is bigger than 2 - make it 2
	{
		n = 2;
	}

	_size = 0;
	_capacity = n; 
	_resizeFactor = n;
	_elements = new int[n];
}

/*
	Input: none
	The function is d'tor that deletes the elements array
*/
Vector::~Vector()
{
	delete[] _elements;
}

/*
	Input: none
	Output: int
	The function returns the elements size
*/
int Vector::size()
{
	return _size;
}

/*
Input: none
Output: int
The function returns the elements capacity
*/
int Vector::capacity()
{
	return _capacity;
}

/*
Input: none
Output: int
The function returns the elements resize factor
*/
int Vector::resizeFactor()
{
	return _resizeFactor;
}

/*
Input: none
Output: bool
The function returns if the elements array is empty
*/
bool Vector::empty()
{
	return (_size == 0);
}

/*
	Input: an integer
	Output: none
	The function checking if the capacity is smaller than the wanted value (n)
	If the capacity is smaller, the function changes the capacity into a bigger value using resizeFactor,
	and then creats a new array of elements and transforming all the values from the old array to the new one.
*/
void Vector::reserve(int n)
{
	if (n > _capacity) //If the wanted size is bigger than the capacity
	{
		int* tempElements = _elements;
		
		//Make new capacity with adding the resize factor until the new capacity is the wanted size
		int newCapacity = _capacity + _resizeFactor;
		while (n > newCapacity)
		{
			newCapacity += _resizeFactor;
		}

		//initiate the elements array and the capacity into the updated values
		_elements = new int[newCapacity];
		_capacity = newCapacity;

		//Transform all the old values of the elements array into the new array with the new capacity
		for (int i = 0; i < _size; i++)
		{
			_elements[i] = tempElements[i];
		}

		delete[] tempElements; //Free the temporary array of elements
	}
}

/*
	Input: an integer value
	Output: none
	The function adding another value into the elements array
*/
void Vector::push_back(const int& val)
{
	if (_size == _capacity) //Check if there is no free space in the elements array
	{
		reserve(_capacity + _resizeFactor); //Change the capacity into a bigger one
	}

	_elements[_size] = val; //Add the new value
	_size++; //Increse the size by 1
}

/*
	Input: none
	Output: the value at the end of the elements
*/
int Vector::pop_back()
{
	int ans = 0;

	if (_size == 0)
	{
		ans = -9999;
		std::cout << "error: pop from empty vector" << endl;
	}
	else
	{
		_size--;
		ans = _elements[_size];
	}

	return ans;
}

/*
	Input: integer
	Output: none
	The function changes the size of the array into n
*/
void Vector::resize(int n)
{
	reserve(n); //Check if n is a valid size, if not - make it suitible to be the size of the array
	_size = n;
}

/*
	Input: an integer
	Output: none
	The function pushes into all the array values the same value
*/
void Vector::assign(int val)
{
	for (int i = 0; i < _size; i++) //Go through all the elements
	{
		_elements[i] = val;
	}
}

/*
	Input: an integer and a value
	Output: none
	The function changes the size of the elements into n,
	and all the new values in the array will be equal to val
*/
void Vector::resize(int n, const int& val)
{
	int oldSize = _size; 
	resize(n); 
	
	for (int i = oldSize; i < _size; i++) //Go through all the new elements
	{
		_elements[i] = val;
	}
}


/*
Input: reference to an object
The copy c'tor creats a new object and compare the fields of the 2 objects
*/
Vector::Vector(const Vector& other)
{
	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;
	_size = other._size;
	_elements = new int[_size];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

/*
Input: reference to an object
Output: reference of a vector
Operator= changes the fields of the object and returns it
*/
Vector& Vector::operator=(const Vector& other)
{
	delete[] _elements;

	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;
	_size = other._size;

	_elements = new int[_size];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}

	return *this;
}

/*
Input: an integer
Output: a reference to an element in the vector
Operator[] will return a reference to an index in element
*/
int& Vector::operator[](int n) const
{
	if (n >= _size) // Checks if the parameter is bigger than the size of the vector
	{
		std::cout << "Error: The parameter is larger than the accessible array";
		n = 0;
	}

	return _elements[n];
}
