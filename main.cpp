/*#include "Vector.h"
#include <iostream>

using namespace std;

int main()
{
	Vector(1);
	Vector v(5);
	int x = 2;
	int& y = x;
	std::cout << v.size();
	std::cout << v.capacity();
	std::cout << v.resizeFactor();
	std::cout << v.empty() << std::endl;
	v.push_back(9);
	std::cout << v.pop_back();
	v.reserve(10);
	v.resize(7);
	v.assign(4);
	v.resize(1, y);

	system("PAUSE");
	return 0;
}*/

#include "Vector.h"
#include <iostream>
using namespace std;

int main()
{
	Vector *v = new Vector(3);
	v->push_back(1);
	v->push_back(2);
	v->push_back(3);
	v->push_back(4);
	v->push_back(5);
	v->push_back(6);
	v->push_back(7);
	v->push_back(8);
	v->push_back(9);

	cout << "need to be print 987654321      ";

	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back();
	cout << v->pop_back() << endl; //should print 987654321


	v->push_back(1);
	v->push_back(2);
	v->push_back(3);
	v->push_back(4);
	v->push_back(5);

	v->assign(0);
	Vector *v1 = new Vector(*v);
	cout << "need to be print 00000          ";
	cout << v1->pop_back();	cout << v1->pop_back();	cout << v1->pop_back();	cout << v1->pop_back();	cout << v1->pop_back() << endl; //should print 00000
																																	//delete v1;

	v->push_back(1);
	v->push_back(2);
	v->push_back(3);
	v->push_back(4);
	v->push_back(5);

	cout << "need to be print 0000012345     ";
	for (int i = 0; i < v->size(); i++)
	{
		cout << (*v)[i];
	}
	cout << endl;

	v->assign(3);
	Vector v2 = *v;

	cout << "need to be print 33333          ";
	cout << v2.pop_back();
	cout << v2.pop_back();
	cout << v2.pop_back();	cout << v2.pop_back();	cout << v2.pop_back() << endl; // should print 33333

	cout << "need to be print 3333333333     ";
	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back();
	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back();	cout << v->pop_back() << endl;// should print 3333333333


	cout << "need to be print 0              ";
	cout << v->size() << endl; // should print 0

	cout << "need to be print 12             ";
	cout << v->capacity() << endl; // should print 12

	v->reserve(31);

	cout << "need to be print 33             ";
	cout << v->capacity() << endl; // should print 12

	cout << "need to be print error          ";
	cout << (*v)[10] << endl;

	cout << "need to be print error          ";
	cout << v->pop_back() << endl << endl; //try to pop from empty vector


	delete v;



	system("PAUSE");
	return 0;
}

